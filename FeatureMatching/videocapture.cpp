#include "opencv2/opencv.hpp"
#include <stdio.h>
#include <iostream>
#include <stdio.h>
#include <iostream>
#include "opencv2/core.hpp"
#include "opencv2/features2d.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/xfeatures2d.hpp"
 
int main(int argh, char* argv[])
{
    std::time_t time_now = std::time(nullptr);
	std::tm time_local_now = *std::localtime(&time_now);
	std::stringstream ss;

	ss
		<< time_local_now.tm_year << "-"
		<< time_local_now.tm_mon + 1 << "-"
		<< time_local_now.tm_mday << "_"
		<< time_local_now.tm_hour << "-"
		<< time_local_now.tm_min << "-"
		<< time_local_now.tm_sec;


    cv::VideoCapture cap(1);//デバイスのオープン
    //cap.open(0);//こっちでも良い．
 
    if(!cap.isOpened())//カメラデバイスが正常にオープンしたか確認．
    {
        //読み込みに失敗したときの処理
        return -1;
    }
 
    while(1)//無限ループ
    {
        cv::Mat frame[10];
		for (int i = 0; i < 10; ){
			cap >> frame[i]; // get a new frame from camera

			//
			//取得したフレーム画像に対して，クレースケール変換や2値化などの処理を書き込む．
			//

			cv::imshow("window", frame[i]);//画像を表示．

			int key = cv::waitKey(1);
			if (key == 113)//qボタンが押されたとき
			{
				return 0;//whileループから抜ける．
			}
			else if (key == 115)//sが押されたとき
			{
				std::stringstream ss_f;
				ss_f << i;
				//フレーム画像を保存する．
				cv::imwrite("img" + ss_f.str() + ".png", frame[i]);
				i++;
			}
			
		}
    }
    cv::destroyAllWindows();
    return 0;
}